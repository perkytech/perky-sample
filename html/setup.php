<?

	//DEVELOPMENT OR PRODUCTION
	$dev = true;
	$environment = $dev ? 'perky.dev' : 'perky.co';

?>
<!doctype html>
	<html>
		<head>
		<meta charset="utf-8">
		<title>Benefits Manager</title>
			<script>
				var pm_settings = { 
					'url':'/connector', 
					'init': 'package/_demo/edit', 
					'on_start': 'pkg.set', 
					'on_save': function () { window.close(); },
					'on_error': function () { location.href = '/error'; }
					}; 
			 </script>
			<script src="https://api.<?=$environment;?>/pm-js"></script>
			<link href="https://api.<?=$environment;?>/pm-css" rel="stylesheet" type="text/css">
			<link href="/theme.css" rel="stylesheet" type="text/css">
		</head>
		<body>
			<div id='header'><div id='logo'></div></div>
		</body>
	</html>
