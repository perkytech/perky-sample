<?
	//header("HTTP/1.0 301 Moved Permanently");
	//header("HTTP/1.0 404 Not Found");
	//header("HTTP/1.0 500 Internal Server Error");
	
	if(isset($_POST['events'])) {

		$events = json_decode(stripslashes($_POST['events']));

		if(is_array($events) && count($events)) { 
	
			foreach($events as $event) {
	
				//EVENT TYPE?
				switch($event->type) {
				
					case 'package':
			
						switch($event->action) {
							
							case 'add': 
								/* DO SOMETHING */ 
								wlog("Data for Package ".$event->package." has been added on ".date('F jS, Y, g:i a', strtotime($event->ts))); 
							break;
							
							case 'update': 
								/* DO SOMETHING */ 
								wlog("Data for Package ".$event->package." has been updated on ".date('F jS, Y, g:i a', strtotime($event->ts))); 
							break;
							
							case 'delete': 
								/* DO SOMETHING */ 
								wlog("Data for Package ".$event->package." has been deleted on ".date('F jS, Y, g:i a', strtotime($event->ts))); 
							break;
							
						}
						
					break;
			
					case 'session': 
					
						switch($event->action) {
					
							case 'delete': 
								/* DO SOMETHING */ 
								wlog("Session Data for User ".$event->user." has been deleted for Package ".$event->package." on ".date('F jS, Y, g:i a', strtotime($event->ts))); 
							break;
							
							case '': 
								/* DO SOMETHING */ 
								wlog("Session Data for User ".$event->user." has been sent for Package ".$event->package." on ".date('F jS, Y, g:i a', strtotime($event->ts))); 
							break;
					
						}
					
					break;
					
					case 'guide': 
													

						/* DO SOMETHING */ 
						wlog("Guide has been completed for User ".$event->user." for Package ".$event->package." on ".date('F jS, Y, g:i a', strtotime($event->ts))); 

					
					break;
			
			
				} 
			}
			
		}
}

function wlog($str='') {
	file_put_contents('log.txt', $str.PHP_EOL, FILE_APPEND);	
}

?>
