<!doctype html>
<html>
	<head>
	<meta charset="utf-8">
	<title>PERKY BENEFITS</title>
	<link href='https://api.<?=$environment;?>/pg-css' rel='stylesheet' type='text/css'>
    <meta name = "viewport" content = "initial-scale=1.0, maximum-scale=1.0, width=device-width">  
	</head>
	<body class='error'>
		<h1>There was a problem starting the Guide...</h1>
		<h2><?= is_object($res) ? $res->code.': '. (isset($res->errors) ? $res->errors[0] : $res->error) : $exe; ?></h2>
	</body>
</html>