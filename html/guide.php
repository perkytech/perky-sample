<?

	//DEVELOPMENT OR PRODUCTION
	$dev = true;
	$environment = $dev ? 'perky.dev' : 'perky.co';

    if($_POST['package']) {
        
        //SETUP DATA (NEED EITHER ref or package PROPERTY TO REFERENCE PACKAGE DATA, ELIG ONLY IF NEEDED)
        $data = (object) array('ref'=>'', 'package'=>$_POST['package'], 'user'=>$_POST['user'], 'return_url'=>'/');

        //PRIMARY MEMBER (Required)
        $data->members[] = array('name'=>$_POST['name'], 'relation'=>'self', 'gender'=>$_POST['gender'], 'dob'=>$_POST['dob']);

        //OPTIONAL DEPENDENTS
        //$data->members[] = array('name'=>'April', 'relation'=>'child', 'gender'=>'female', 'dob'=>'2010-02-24');
        //$data->members[] = array('name'=>'Mary', 'relation'=>'spouse', 'gender'=>'female', 'dob'=>'1983-03-04');

        $json = json_encode($data);
        $curl = curl_init();

        curl_setopt_array($curl, 
            array(
                CURLOPT_RETURNTRANSFER => 1, 
                CURLOPT_URL => $_SERVER['HTTP_HOST'].'/connector.php', 
                CURLOPT_POST => 1, 
                CURLOPT_SSL_VERIFYHOST=> false, 
                CURLOPT_SSL_VERIFYPEER=>false,
								CURLOPT_HTTPHEADER => array("X-Requested-With: XMLHttpRequest"),
                CURLOPT_POSTFIELDS => array('endpoint'=>'session', 'data'=>$json)
            )
        );

        $exe = curl_exec($curl);	
        $res = json_decode($exe);

        curl_close($curl);

        if(is_object($res) && property_exists($res, 'token')) { 
            include('inc/guide-start.inc');
        } else {
            include('inc/guide-error.inc');
        }
    
    } else { 
        include('inc/guide-demo.inc');
    }

?>
